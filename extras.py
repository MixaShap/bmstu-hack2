import numpy as np


class Schema_opt:

    def __init__(self, matrix, containers):
        """Constructor"""
        self.containers_size = containers
        self.el_amount = len(matrix[0])
        self.matr = matrix
        self.containers_variants = []

    def combinationSum(cont_size, el_amount, res):
        dfs(cont_size, el_amount, 0, [], res)
        return res

    def dfs(nums, summa, index, path, res):
        if summa < 0:
            return
        if summa == 0:
            res.append(path)
            return
        for i in range(index, len(nums)):
            dfs(nums, summa - nums[i], i, path + [nums[i]], res)

    def weight_of_str(matrix, i):
        summa = 0
        for j in range(len(matrix[i])):
            summa += matrix[i][j]
        return summa

    def first_candidate(matrix):
        weights = []
        for i in range(len(matrix)):
            weights.append(weight_of_str(matrix, i))
        print('Вектор сумм:')
        for i in range(len(weights)):
            print('V{}:'.format(i), weights[i])
        return (np.array(weights).argmax())

    def search_for_incident(cand_index, matrix):
        subset = [cand_index]
        for i in range(len(matrix[cand_index])):
            if matrix[cand_index][i] > 0:
                subset.append(i)
        return subset

    def delta_function(subset, matrix):
        delta_funcs = []
        for i in range(len(subset)):
            delta = weight_of_str(matrix, subset[i])
            for j in range(len(subset)):
                # delta-=matrix[subset[j]][subset[i]]
                delta -= matrix[subset[i]][subset[j]]
            delta_funcs.append(delta)
        return delta_funcs

    def make_subsets(combination, matrix):
        subsets = []
        while (len(combination) > 0):
            cand_index = first_candidate(matrix)
            print('Первый кандидат:', cand_index)
            subset = search_for_incident(cand_index, matrix)
            print('Инцидентные:', subset)
            while (len(subset) in combination) == False:
                if (len(combination) == 1):
                    nz = np.nonzero(matrix)
                    indexes = np.c_[nz]
                    subset = list(set(indexes[:, 0]))
                else:
                    delta_funcs = delta_function(subset, matrix)
                    print('Дельта-функции:')
                    for i in range(len(delta_funcs)):
                        print('V{}:'.format(subset[i]), delta_funcs[i])
                    cand_delete = np.array(delta_funcs).argmax()
                    print('Кандидат на удаление:', cand_delete)
                    subset.pop(cand_delete)
            print('Подмножество', subset)
            subsets.append(subset)
            combination.remove(len(subset))
            for j in range(len(subset)):
                vertex_index = subset[j]
                matrix[vertex_index] = 0
                matrix[:, vertex_index] = 0
        return (subsets)

    def first_composition(matrix):
        combinationSum(containers_size, el_amount, containers_variants)
        print('Количество комбинаций контейнеров: ', len(containers_variants))
        '''
        for i in range(len(containers_variants)):
                #print('Комбинация {}'.format(i), containers_variants[i])
                print(containers_variants[i])
                S_temp=matrix.copy()
                containers_variant_temp=containers_variants[i].copy()
                subsets=make_subsets(containers_variant_temp, S_temp)
                #print('Полученные подможества вершин:',subsets)
        '''
        S_temp = matrix.copy()
        containers_variant_temp = containers_variants[38].copy()
        subsets = make_subsets(containers_variant_temp, S_temp)

    def new_matrix(matrix, subsets):
        S_start = np.zeros((el_amount, el_amount))
        for i, index in enumerate(indexes_order):
            for j in range(len(matrix[index])):
                if matrix[index][j] != 0:
                    id = np.argwhere(indexes_order == j)[0]
                    S_start[index][id] = matrix[index][j]
        return (S_start)

    def target_func(matrix, subsets):
        sum = 0
        start_str = 0
        end_str = 0
        start_col = 0
        for i in range(len(subsets)):
            size = len(subsets[i])
            end_str += size
            start_col += size
            for j in range(len(matrix[start_str:end_str])):
                for id, num in enumerate(matrix[start_str + j][start_col:]):
                    sum += num
            start_str += size
        return (sum)

    def R_matrix(matrix, subsets):
        iter = 0
        col_amount = len(indexes_order)
        vertex1 = 0
        vertex2 = len(subsets[0])
        while True:
            subset1 = subsets[0].copy()
            col_amount -= len(subset1)
            R = np.zeros((len(subset1), col_amount))
            if iter == 1:
                vertex2 += len(subset1)
            place1 = 0
            for i in range(len(subset1)):
                place2 = 0
                for subset2 in subsets[1:]:
                    for j in range(len(subset2)):
                        delta_func = 0
                        delta_func += sum(matrix[vertex1][vertex2:vertex2 + len(subset2)])
                        delta_func -= sum(matrix[vertex1][vertex1 - i:vertex1 + len(subset1) - i])
                        delta_func += sum(matrix[vertex2 + j][vertex1 - i:vertex1 + len(subset1) - i])
                        delta_func -= sum(matrix[vertex2 + j][vertex2:vertex2 + len(subset2)])
                        delta_func -= 2 * matrix[vertex1][vertex2 + j]
                        R[place1][place2] = delta_func
                        place2 += 1
                    vertex2 += len(subset2)
                for subset2 in subsets[1:]:
                    vertex2 -= len(subset2)
                vertex1 += 1
                place1 += 1
            print('Матрица внешних связей R:\n', R)
            if R.max() > 0:
                id1 = np.argwhere(R == R.max())[0][0] + iter * (vertex1 - len(subset1))
                id2 = np.argwhere(R == R.max())[0][1] + vertex2
                print('Наибольший положительный элемент марицы R: ', R.max())
                print('Его индексы в матрице S:', indexes_order[id1], indexes_order[id2])
                matrix[:, id1], matrix[:, id2] = matrix[:, id2], matrix[:, id1].copy()
                matrix[id1], matrix[id2] = matrix[id2], matrix[id1].copy()
                indexes_order[id1], indexes_order[id2] = indexes_order[id2], indexes_order[id1]
                break
            else:
                iter = 1
                del subsets[0]
                if len(subsets) == 1:
                    break

    first_composition(matr)
    print('\n\nНаилучшая комбинация - 38:', containers_variants[38])
    subsets = make_subsets(containers_variants[38], matr.copy())
    # subsets=[[0,1],[2,3,4],[5,6,7]]
    print('Начальное разбиение:', subsets)
    '''
    indexes_order=[]
    for i in range(len(subsets)):
        for j in range(len(subsets[i])):
            indexes_order.append(subsets[i][j])
    indexes_order = np.array(indexes_order)
    S_start=new_matrix(S,subsets)
    print(S_start)
    #Q=target_func(S_start,subsets)
    #print(Q)
    Q_prev=target_func(S_start,subsets)
    print('Значение целевой функции после первичного размещения:', Q_prev)
    while True:
        R_matrix(S_start,subsets.copy())
        id=0
        for i in range(len(subsets)):
            for j in range(len(subsets[i])):
                subsets[i][j]=indexes_order[id]
                id += 1
        Q_cur=target_func(S_start,subsets)
        print('Значение целевой функции после итерационного алгоритма:', Q_cur)
        if Q_cur >= Q_prev:
            print('Значение целевой функции не улучшилось на данном шаге. Прекращение работы алгоритма.')
            break
        print('Исходная матрица после выполненной итерации:\n',S_start)
        print('Контейнеры после выполненной итерации: ',subsets)
        Q_prev = Q_cur
    '''
