import numpy as np
import extras


FILENAME = "./res/dots.txt"
containers_size = [3, 4, 5, 6, 7]

if __name__ == '__main__':
    adjacency_matrix = np.loadtxt(FILENAME, delimiter=',')
    print(adjacency_matrix)             # array
    print(len(adjacency_matrix[0]))     # amount of elements

