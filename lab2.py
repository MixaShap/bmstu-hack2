import numpy as np
import math


FILENAME = "./res/dots2.txt"

S = np.loadtxt(FILENAME, delimiter=',')

V = np.array([[0, 0, 0, 3, 0, 0, 2, 3, 0],
              [0, 0, 2, 0, 2, 0, 0, 0, 0],
              [0, 2, 0, 1, 0, 0, 0, 0, 0],
              [3, 0, 1, 0, 0, 5, 0, 0, 0],
              [0, 2, 0, 0, 0, 2, 0, 0, 4],
              [0, 0, 0, 5, 2, 0, 5, 0, 0],
              [2, 0, 0, 0, 0, 5, 0, 6, 2],
              [3, 0, 0, 0, 0, 0, 6, 0, 0],
              [0, 0, 0, 0, 4, 0, 2, 0, 0]])


def vertex_contacts(matrix, i):
    summa = 0
    for j in range(len(matrix[i])):
        summa += matrix[i][j]
    return summa


def connectivity_coeff(matrix, i):
    coeff = 0
    for j, num in enumerate(el_placed):
        coeff += matrix[i][num]
    v_contacts.append(vertex_contacts(matrix, i))
    coeff = coeff * 2 - v_contacts[-1]
    return coeff


def distance(vertex1, vertex2):
    return (math.sqrt((vertex1[0] - vertex2[0]) ** 2 + (vertex1[1] - vertex2[1]) ** 2))


def first_placing(matrix):
    global nearest_places
    global el_placed
    global connectivity_coeffs
    summa = []
    for i in range(len(matrix)):
        sum_str = 0
        for j in range(len(matrix[i])):
            sum_str += matrix[i][j]
        summa.append(sum_str)
    print(summa)
    for idr, row in enumerate(plata):
        for idc, col in enumerate(row):
            dist = distance([0, 0], [idr, idc])
            nearest_places.append((idr, idc, dist))
    nearest_places = sorted(nearest_places, key=lambda x: x[2])
    nearest_places = [coordinates[:-1] for coordinates in nearest_places]
    plata[0][0] = 1
    el_placed = [0]
    nearest_places.pop(0)
    while plata[-1][-1] == 0:
        for i in range(len(matrix)):
            coeff = connectivity_coeff(matrix, i)
            connectivity_coeffs.append(coeff)
        connectivity_coeffs = [min(connectivity_coeffs) if id in el_placed else num for id, num in
                               enumerate(connectivity_coeffs)]
        print('Коэффициенты связности:', connectivity_coeffs)
        candidates = [id for id, num in enumerate(connectivity_coeffs) if num == max(connectivity_coeffs)]
        print('Кандидаты:', candidates)
        for id, num in enumerate(candidates):
            y = nearest_places[0][0]
            x = nearest_places[0][1]
            plata[y][x] = num + 1
            nearest_places.pop(0)
        print(plata)
        el_placed += candidates
        connectivity_coeffs.clear()
        candidates.clear()
    return plata


def target_func(matrix):
    Q = 0
    plata_places.clear()
    for i in range(len(matrix)):
        place1 = np.argwhere(plata == i + 1)[0]
        plata_places.append(place1)
        q = 0
        for j, num in enumerate(matrix[i]):
            if num != 0:
                place2 = np.argwhere(plata == j + 1)[0]
                dist = math.fabs(place1[0] - place2[0]) + math.fabs(place1[1] - place2[1])
                q += dist * num
        Q += q
    Q /= 2
    return Q


def average_cond_length(matrix, i):
    av_length = 0
    for id, num in enumerate(matrix[i]):
        if num != 0:
            dist = math.fabs(plata_places[i][0] - plata_places[id][0]) + math.fabs(
                plata_places[i][1] - plata_places[id][1])
            av_length += dist * num
    av_length /= v_contacts[i]
    return (av_length)


def center_mass(matrix):
    global av_cond_length
    av_cond_length.clear()
    for i in range(len(matrix)):
        L = average_cond_length(matrix, i)
        av_cond_length.append(L)
    print('Вектор L:', av_cond_length)
    v_index = np.array(av_cond_length).argmax()
    x = 0
    y = 0
    for id, num in enumerate(matrix[v_index]):
        if num != 0:
            x += (plata_places[id][1] + 1) * num
            y += (len(plata) - plata_places[id][0]) * num
    x /= v_contacts[v_index]
    y /= v_contacts[v_index]
    return y, x, v_index


def center_neihbors(matrix):
    y, x, v = center_mass(matrix)
    print('Центр масс определяется по вершине:', v + 1)
    print('Координаты центра масс (x,y):', x, y)
    if y.is_integer() and x.is_integer(): return
    x1 = int(round(x) - 1)
    y1 = int(round(y) - 1)
    candidates = [v]
    candidates.append([y1, x1])
    if x.is_integer():
        if y1 + 1 < y:
            y2 = y1 + 1
            candidates.append([y2, x1])
        else:
            y2 = y1 - 1
            candidates.append([y2, x1])
    elif y.is_integer():
        if x1 + 1 < x:
            x2 = x1 + 1
            candidates.append([y1, x2])
        else:
            x2 = x1 - 1
            candidates.append([y1, x2])
    else:
        if x1 + 1 < x:
            x2 = x1 + 1
            candidates.append([y1, x2])
        else:
            x2 = x1 - 1
            candidates.append([y1, x2])
        if y1 + 1 < y:
            y2 = y1 + 1
            candidates.append([y2, x1])
        else:
            y2 = y1 - 1
            candidates.append([y2, x1])
        candidates.append([y2, x2])
    return candidates


def change(matrix):
    candidates = center_neihbors(matrix)
    id1 = candidates[0]
    y1 = plata_places[id1][0]
    x1 = plata_places[id1][1]
    L1_init = av_cond_length[id1]
    deltas = []
    print('Кандидаты на перемещение:')
    for i in range(1, len(candidates)):
        y2 = len(plata) - 1 - candidates[i][0]
        x2 = candidates[i][1]
        id2 = int(plata[y2][x2]) - 1
        print(id2 + 1)
        L2_init = av_cond_length[id2]
        plata_places[id1] = [y2, x2]
        plata_places[id2] = [y1, x1]
        L1_new = average_cond_length(matrix, id1)
        L2_new = average_cond_length(matrix, id2)
        delta = (L1_new - L1_init) + (L2_new - L2_init)
        deltas.append(delta)
        plata_places[id1] = [y1, x1]
        plata_places[id2] = [y2, x2]
    print('Вектор дельт:', deltas)
    if min(deltas) < 0:
        v = np.array(deltas).argmin()
        y2 = len(plata) - 1 - candidates[v + 1][0]
        x2 = candidates[v + 1][1]
        id2 = int(plata[y2][x2])
        plata[y2][x2] = id1 + 1
        plata[y1][x1] = id2
    return


def iterations(matrix):
    Q_prev = target_func(matrix)
    print('Значение целевой функции после первичного размещения:', Q_prev)
    Q_cur = 0
    while True:
        change(matrix)
        Q_cur = target_func(matrix)
        print('Значение целевой функции после итерационного алгоритма:', Q_cur)
        if Q_cur >= Q_prev:
            print('Значение целевой функции не улучшилось на данном шаге. Прекращение работы алгоритма.')
            break
        print("Плата после применения текущего шага итерационного алгоритма:\n", plata)
        Q_prev = Q_cur


plata = np.zeros((5, 6))
nearest_places = []
v_contacts = []
connectivity_coeffs = []
plata_places = []
av_cond_length = []
plata = first_placing(S)
print("Плата после первичного размещения:\n", plata)
iterations(S)
